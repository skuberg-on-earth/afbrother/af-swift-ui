
import Foundation
import SwiftUI

@available(iOS 13.0, *)
struct imageAd: View{
    var appKey:String = ""
    var adKey:String = ""
    var adType:String = ""
    @ObservedObject var imageLoader:ImageLoader
    @State var image:UIImage = UIImage()
    @State var width:Double = 0
    @State var height:Double = 0
    @State var href:String = ""
    
    init(appKey: String, adKey: String,adType:String){
        self.appKey = appKey
        self.adKey = adKey
        self.adType = adType
        self.imageLoader = ImageLoader(appKey: self.appKey, adKey: self.adKey)
    }
    
    func newBody () -> some View {
        let windowWidth:Double = UIScreen.screenWidth
        var adScale:Double
        if(windowWidth != 0 && width != 0){
            adScale = windowWidth/width
        }else{
            adScale = 1
        }
        
        if(adType=="inpage"||adType=="slide"){
            return Button(action: {
                guard let url = URL(string: self.href),
                      UIApplication.shared.canOpenURL(url) else {
                    return
                }
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }){
                Image(uiImage: image)
                    .resizable()
                    .frame(width:width, height:height)
                    .onReceive(imageLoader.didChange) { data in
                        self.image = UIImage(data: data) ?? UIImage()
                        self.width = imageLoader.width
                        self.height = imageLoader.height
                        self.href = imageLoader.href
                    }
            }
        }else {
            return Button(action: {
                guard let url = URL(string: self.href),
                      UIApplication.shared.canOpenURL(url) else {
                    return
                }
                UIApplication.shared.open(url,
                                          options: [:],
                                          completionHandler: nil)
            }){
                Image(uiImage: image)
                    .resizable()
                    .frame(width:windowWidth, height:height*adScale)
                    .onReceive(imageLoader.didChange) { data in
                        self.image = UIImage(data: data) ?? UIImage()
                        self.width = imageLoader.width
                        self.height = imageLoader.height
                        self.href = imageLoader.href
                    }
            }
        }
    }
    var body: some View {
        newBody()
    }
}

extension UIScreen{
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let screenSize = UIScreen.main.bounds.size
}

@available(iOS 13.0, *)
extension View {
    @ViewBuilder func isHidden(_ hidden: Bool, remove: Bool = false) -> some View {
        if hidden {
            if !remove {
                self.hidden()
            }
        } else {
            self
        }
    }
}

@available(iOS 13.0, *)
struct renderAd: View {
    
    var appKey:String = ""
    var adKey:String = ""
    var adType:String = ""
    @ObservedObject var imageLoader:ImageLoader
    @State var image:UIImage = UIImage()
    @State var width:Double = 0
    @State var height:Double = 0
    @State var href:String = ""
    @State var adClose:Bool = false
    
    init(appKey: String, adKey: String, adType:String){
        self.appKey = appKey
        self.adKey = adKey
        self.adType = adType
        self.imageLoader = ImageLoader(appKey: "close", adKey: "close")
    }
    
    var body: some View {
        if #available(iOS 15.0, *) {
            imageAd(appKey: self.appKey, adKey: self.adKey,adType:self.adType)
                .isHidden(self.adClose,remove: self.adClose)
                .overlay {
                    HStack {
                        VStack(alignment: .leading) {
                            Button(action: {
                                self.adClose = true
                            }){
                                Image(uiImage: image)
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width:width, height:height)
                                    .isHidden(self.adClose,remove: self.adClose)
                                    .background(Color.white.opacity(0.1))
                                    .onReceive(imageLoader.didChange) { data in
                                        self.image = UIImage(data: data) ?? UIImage()
                                        self.width = imageLoader.width
                                        self.height = imageLoader.height
                                        self.href = imageLoader.href
                                    }
                            }
                            Spacer()
                        }
                        Spacer()
                    }
                }
        } else {
            // Fallback on earlier versions
        }
    }
}
